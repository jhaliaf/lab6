/*****************
Jha'Liah Frierson
jhaliaf
Lab 5
Lab Section: 006
Anurata Hridi
*****************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  //Creates a deck of cards of size 52
  Card deck[52];
  int s;
  int pos = 0;
  //Loops 2 through Ace of the deck
  for(int i = 2; i <= 14; i++)
  {
    //Loops through the suits spades through clubs
    //Suit values assigned in enum above
    for(s = 0; s <= 3 ; s++)
    {
    //Assigns the suit and value of each card
    //Increments the position in the deck 
    deck[pos].suit = static_cast<Suit>(s);
    deck[pos].value = i;
    pos++;
    }
  }


  /*After the deck is created and initialzed we call random_shuffle(), this
   function will shuffle the deck.*/
  
  random_shuffle(&deck[0], &deck[52], &myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
    sort(&hand[0], &hand[5], &suit_order);
  
    /*The following will print the cards in the correct order*/ 
    for(int i = 0; i <= 4; i++)
    { 
      //Displays the hand of cards, right aligned and with a width of 10
      cout << setw(10) << get_card_name(hand[i]) << " " 
      << "of " << get_suit_code(hand[i]) << endl;
  }


  return 0;
}


/*This function will be passed to the sort funtion. This function returns
* true or false based on the if statements below*/
bool suit_order(const Card& lhs, const Card& rhs) {
  //If the right hand suit is less the left hand suit returns true
  if(lhs.suit < rhs.suit)
  {
    return true;
  }
  //If the suits are the same compare the values of each card
  else if ((lhs.suit == rhs.suit) &&(lhs.value < rhs.value))
  {
        return true;
    }
  else if ((lhs.suit == rhs.suit) &&(lhs.value > rhs.value))
    {
       return false;
    }
  //If the left hand suit is less the right hand suit returns false
  else
  {
    return false;
  }
}

//This function returns the actual character of the suits
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}
//This function returns the name of the card based on the value
string get_card_name(Card& c) {
  switch (c.value){
  case 2:	return "2";
  case 3:	return "3";
  case 4:	return "4";
  case 5:	return "5";
  case 6:	return "6"; 
  case 7:	return "7";
  case 8:	return "8";
  case 9:	return "9";
  case 10:	return "10";
  case 11:	return "Jack";
  case 12:	return "Queen";
  case 13:	return "King";
  case 14:	return "Ace";
  default:	return "";
  }
}
